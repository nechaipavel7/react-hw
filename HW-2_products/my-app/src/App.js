import { Component } from "react";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import ProductList from "./components/ProductList/ProductList";

class App extends Component {
  state = {
    products: [],
    isOpenModalFirst: false,
    cartCounter: 0,
    favoriteCounter: 0,
  };

  addToCart = () => {
    this.setState((current) => {
      localStorage.setItem("cartCounter", current.cartCounter + 1);
      return { cartCounter: current.cartCounter + 1, isOpenModalFirst: false };
    });
  };

  addToFavorites = (id) => {
    this.setState((current) => {
      const newState = { ...current };
      const index = current.products.findIndex((el) => id === el.id);

      if (!newState.products[index].isFavorite) {
        newState.products[index].isFavorite = true;

        localStorage.setItem("products", JSON.stringify(newState.products));

        newState.favoriteCounter += 1;
        localStorage.setItem("favoriteCounter", newState.favoriteCounter);
      }

      return newState;
    });
  };

  async componentDidMount() {
    const products = localStorage.getItem("products");
    if (products) {
      this.setState({ products: JSON.parse(products) });
    } else {
      const products = await fetch("./products.json").then((res) => res.json());
      localStorage.setItem("products", JSON.stringify(products));
      this.setState({ products: products });
    }

    const cartCounter = localStorage.getItem("cartCounter");
    if (cartCounter) {
      this.setState({ cartCounter: JSON.parse(cartCounter) });
    }

    const favoriteCounter = localStorage.getItem("favoriteCounter");
    if (favoriteCounter) {
      this.setState({ favoriteCounter: JSON.parse(favoriteCounter) });
    }
  }

  statusModal = (value) => {
    this.setState({ isOpenModalFirst: value });
  };

  render() {
    const { favoriteCounter, cartCounter, products, isOpenModalFirst } =
      this.state;
    return (
      <div className={styles.App}>
        <Header cartCounter={cartCounter} favoriteCounter={favoriteCounter} />
        <main>
          <section>
            <ProductList
              addToFavorites={this.addToFavorites}
              statusModal={this.statusModal}
              products={products}
            />
          </section>
        </main>

        {isOpenModalFirst && (
          <Modal
            closeButton="X"
            onClick={() => {
              this.statusModal(false);
            }}
            header="Додати обраний товар до кошика?"
            text="Натиснувши `ДО КОШИКА` ви додасте цей товар у свій кошик"
            action={
              <div className={styles.btnContainer}>
                <Button
                  className={styles.btnModal}
                  text="Продовжити покупки"
                  onClick={() => {
                    this.statusModal(false);
                  }}
                  backgroundColor="#7A7A7A"
                />
                <Button
                  className={styles.btnModal}
                  onClick={this.addToCart}
                  text="ДО КОШИКА"
                  backgroundColor="#001542"
                />
              </div>
            }
          />
        )}
      </div>
    );
  }
}

export default App;
