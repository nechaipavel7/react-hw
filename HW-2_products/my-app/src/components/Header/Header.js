import { PureComponent } from "react";
import styles from "./Header.module.scss";
import PropTypes from "prop-types";

class Header extends PureComponent {
  render() {
    const { cartCounter, favoriteCounter } = this.props;
    return (
      <header className={styles.header}>
        <div>
          <img
            className={styles.logo_img}
            src="./images/logo-header.png"
            alt="logo"
          ></img>
        </div>
        <div>
          <ul>
            <li>
              <a href="">Доставка і оплата</a>
            </li>
            <li>
              <a href="">Повернення і обмін</a>
            </li>
            <li>
              <a href="">Гарантія</a>
            </li>
          </ul>
        </div>
        <div className={styles.iconHeader}>
          <span className={styles.cart}>
            <a href="#">
              <img
                className={styles.cartImg}
                src="./images/favorite-check.png"
                alt="star"
              />
            </a>

            <span> {favoriteCounter}</span>
          </span>

          <span className={styles.cart}>
            <a href="#">
              <img
                className={styles.cartImg}
                src="./images/shopping-cart.png"
                alt="cart"
              />
            </a>
            <span>{cartCounter}</span>
          </span>
        </div>
      </header>
    );
  }
}
Header.propTypes = {
  favoriteCounter: PropTypes.number,
  cartCounter: PropTypes.number,
};

Header.defaultProps = {
  favoriteCounter: 0,
  cartCounter: 0,
};

export default Header;
