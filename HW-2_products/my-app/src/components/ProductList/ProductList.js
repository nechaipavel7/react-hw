import { Component } from "react";
import ProductCard from "../ProductCard/ProductCard";
import styles from "./ProductList.module.scss";
import PropTypes from "prop-types";

class ProductList extends Component {
  render() {
    const { addToFavorites, statusModal, products } = this.props;

    return (
      <div>
        <ul className={styles.list}>
          {products.map(
            ({ id, name, price, url, artcl, color, isFavorite }) => (
              <li key={id}>
                <ProductCard
                  addToFavorites={addToFavorites}
                  statusModal={statusModal}
                  id={id}
                  name={name}
                  price={price}
                  url={url}
                  artcl={artcl}
                  color={color}
                  isFavorite={isFavorite}
                />
              </li>
            )
          )}
        </ul>
      </div>
    );
  }
}

ProductList.propTypes = {
  products: PropTypes.array,
  statusModal: PropTypes.func,
  addToFavorites: PropTypes.func,
};

ProductList.defaultProps = {
  products: [],
  statusModal: () => {},
  addToFavorites: () => {},
};

export default ProductList;
