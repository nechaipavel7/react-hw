import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

class Modal extends React.Component {
  render() {
    const { header, text, closeButton, action, onClick } = this.props;

    // if (!isOpen) {
    //   return null;
    // }

    return (
      <div>
        <div className={styles.backgroundModal} onClick={onClick}></div>
        <div className={styles.modal}>
          <div className={styles.header}>
            <h1 className={styles.headerText}>{header}</h1>
            <button className={styles.closeButton} onClick={onClick}>
              {closeButton}
            </button>
          </div>
          <div>
            <h2 className={styles.textModal}>{text}</h2>
            <div>{action}</div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string,
  closeButton: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  action: PropTypes.element.isRequired,
};

Modal.defaultProps = {
  text: "Add to cart",
  closeButton: "X",
};

export default Modal;
