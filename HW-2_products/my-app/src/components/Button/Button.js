import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

class Button extends React.Component {
  render() {
    const { text, type, onClick, backgroundColor } = this.props;
    return (
      <button
        className={styles.btn}
        onClick={onClick}
        type={type}
        style={{ backgroundColor: backgroundColor }}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  backgroundColor: PropTypes.string,
  type: PropTypes.oneOf(["button", "submit", "reset"]),
};

Button.defaultProps = {
  text: "Add to cart",
  backgroundColor: "red",
  type: "button",
};

export default Button;
