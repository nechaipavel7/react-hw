import React from "react";
import { Routes, Route } from "react-router-dom";
import CartPage from "./pages/CartPage/CartPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import HomePage from "./pages/HomePage/HomePage";
import PropTypes from "prop-types";

const AppRoutes = (props) => {
  const {
    modalState,
    isOpenModalFirst,
    setIsOpenModalFirst,
    setModalState,
    addToFavoritePage,
    deleteCartItem,
    dicrementCartItem,
    incrementCartItem,
    favorites,
    carts,
    addToCartsPage,
    products,
  } = props;

  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            modalState={modalState}
            setModalState={setModalState}
            setIsOpenModalFirst={setIsOpenModalFirst}
            isOpenModalFirst={isOpenModalFirst}
            addToFavoritePage={addToFavoritePage}
            products={products}
            addToCartsPage={addToCartsPage}
          />
        }
      />

      <Route
        path="/cart"
        element={
          <CartPage
            setIsOpenModalFirst={setIsOpenModalFirst}
            isOpenModalFirst={isOpenModalFirst}
            modalState={modalState}
            setModalState={setModalState}
            deleteCartItem={deleteCartItem}
            dicrementCartItem={dicrementCartItem}
            incrementCartItem={incrementCartItem}
            carts={carts}
          />
        }
      />

      <Route
        path="/favorite"
        element={
          <FavoritePage
            isOpenModalFirst={isOpenModalFirst}
            setModalState={setModalState}
            addToCartsPage={addToCartsPage}
            modalState={modalState}
            setIsOpenModalFirst={setIsOpenModalFirst}
            favorites={favorites}
            addToFavoritePage={addToFavoritePage}
          />
        }
      />
    </Routes>
  );
};

AppRoutes.propTypes = {
  modalState: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  isOpenModalFirst: PropTypes.bool,
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  setModalState: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  addToFavoritePage: PropTypes.func,
  deleteCartItem: PropTypes.func,
  dicrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  favorites: PropTypes.array,
  carts: PropTypes.array,
  products: PropTypes.oneOfType([PropTypes.array, PropTypes.number]).isRequired,
  addToCartsPage: PropTypes.func,
};

AppRoutes.defaultProps = {
  modalState: {},
  isOpenModalFirst: false,
  setIsOpenModalFirst: false,
  setModalState: {},
  addToFavoritePage: () => {},
  deleteCartItem: () => {},
  dicrementCartItem: () => {},
  incrementCartItem: () => {},
  favorites: 0,
  carts: 0,
  addToCartsPage: () => {},
  products: [],
};

export default AppRoutes;
