import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

const Modal = (props) => {
  const {
    modalState,
    confirmBtnAction,
    isOpenModalFirst,
    setIsOpenModalFirst,
    text,
  } = props;

  const closeModal = () => {
    setIsOpenModalFirst(false);
  };

  if (!isOpenModalFirst) {
    return null;
  }

  return (
    <div>
      <div className={styles.backgroundModal} onClick={closeModal}></div>
      <div className={styles.modal}>
        <div className={styles.header}>
          <h1 className={styles.headerText}>{modalState.name}</h1>
          <button className={styles.closeButton} onClick={closeModal}>
            X
          </button>
        </div>
        <div>
          <h2 className={styles.textModal}>{text}</h2>
          <div className={styles.btnContainer}>
            <Button
              className={styles.btnModal}
              text="НІ"
              onClick={() => {
                closeModal();
              }}
              backgroundColor="#7A7A7A"
            />
            <Button
              className={styles.btnModal}
              onClick={() => {
                confirmBtnAction(modalState);
                closeModal();
              }}
              text="ТАК"
              backgroundColor="#001542"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  isOpenModalFirst: PropTypes.bool.isRequired,
  modalState: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  confirmBtnAction: PropTypes.func.isRequired,
  text: PropTypes.string,
};

Modal.defaultProps = {
  setIsOpenModalFirst: false,
  modalState: {},
  text: "Add to cart",
};

export default Modal;
