import React from "react";
import Cart from "../Cart/Cart";
import styles from "./CartList.module.scss";
import PropTypes from "prop-types";

const CartList = (props) => {
  const {
    setIsOpenModalFirst,
    setModalState,
    dicrementCartItem,
    incrementCartItem,
    carts,
  } = props;

  return (
    <div>
      <ul className={styles.list}>
        {carts.map(({ id, name, price, url, artcl, color, count }) => (
          <li key={id}>
            <Cart
              setIsOpenModalFirst={setIsOpenModalFirst}
              setModalState={setModalState}
              dicrementCartItem={dicrementCartItem}
              incrementCartItem={incrementCartItem}
              count={count}
              id={id}
              name={name}
              price={price}
              url={url}
              artcl={artcl}
              color={color}
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

CartList.propTypes = {
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  setModalState: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  dicrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  carts: PropTypes.array,
};

CartList.defaultProps = {
  setIsOpenModalFirst: false,
  setModalState: {},
  dicrementCartItem: () => {},
  incrementCartItem: () => {},
  carts: [],
};

export default CartList;
