import React from "react";
import Favorite from "../Favorite/Favorite";
import styles from "./FavoriteList.module.scss";
import PropTypes from "prop-types";

const FavoriteList = (props) => {
  const { setModalState, setIsOpenModalFirst, favorites, addToFavoritePage } =
    props;

  return (
    <div>
      <ul className={styles.list}>
        {favorites.map(({ id, name, price, url, artcl, color, isFavorite }) => (
          <li key={id}>
            <Favorite
              setModalState={setModalState}
              setIsOpenModalFirst={setIsOpenModalFirst}
              addToFavoritePage={addToFavoritePage}
              isFavorite={isFavorite}
              id={id}
              name={name}
              price={price}
              url={url}
              artcl={artcl}
              color={color}
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

FavoriteList.propTypes = {
  setModalState: PropTypes.oneOfType([PropTypes.array, PropTypes.func]),
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  favorites: PropTypes.oneOfType([PropTypes.array, PropTypes.number]),
  addToFavoritePage: PropTypes.func,
};

FavoriteList.defaultProps = {
  setModalState: {},
  setIsOpenModalFirst: false,
  favorites: [],
  addToFavoritePage: () => {},
};

export default FavoriteList;
