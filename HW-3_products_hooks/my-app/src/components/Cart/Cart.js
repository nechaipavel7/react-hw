import React from "react";
import styles from "./Cart.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

const Cart = (props) => {
  const {
    setIsOpenModalFirst,
    setModalState,
    dicrementCartItem,
    incrementCartItem,
    id,
    name,
    url,
    count,
  } = props;
  return (
    <div className={styles.productCard}>
      <a>
        <img className={styles.productImg} src={url} alt={name} />
      </a>
      <span className={styles.productName}>{name}</span>

      <div className={styles.btnContainer}>
        <Button
          onClick={() => dicrementCartItem(id)}
          className={styles.btn}
          text="-"
          backgroundColor="#085454"
        ></Button>
        <span className={styles.count_cart}>{count}</span>
        <Button
          onClick={() => incrementCartItem(id)}
          className={styles.btn}
          text="+"
          backgroundColor="#085454"
        ></Button>
        <Button
          onClick={() => {
            setModalState(id);
            setIsOpenModalFirst(true);
          }}
          color="red"
          className={styles.btn}
          text="ВИДАЛИТИ"
        ></Button>
      </div>
    </div>
  );
};

Cart.propTypes = {
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  setModalState: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  dicrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  id: PropTypes.number,
  name: PropTypes.string,
  url: PropTypes.string,
  count: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};

Cart.defaultProps = {
  setIsOpenModalFirst: false,
  setModalState: {},
  dicrementCartItem: () => {},
  incrementCartItem: () => {},
  id: 0,
  name: "Назва товару",
  url: "фото товару",
  count: 0,
};

export default Cart;
