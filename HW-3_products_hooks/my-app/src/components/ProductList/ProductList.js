import React from "react";
import ProductCard from "../ProductCard/ProductCard";
import styles from "./ProductList.module.scss";
import PropTypes from "prop-types";

const ProductList = (props) => {
  const { setModalState, setIsOpenModalFirst, addToFavoritePage, products } =
    props;

  return (
    <div>
      <ul className={styles.list}>
        {products.map(({ id, name, price, url, artcl, color, isFavorite }) => (
          <li key={id}>
            <ProductCard
              setModalState={setModalState}
              setIsOpenModalFirst={setIsOpenModalFirst}
              addToFavoritePage={addToFavoritePage}
              id={id}
              name={name}
              price={price}
              url={url}
              artcl={artcl}
              color={color}
              isFavorite={isFavorite}
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

ProductList.propTypes = {
  setModalState: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  addToFavoritePage: PropTypes.func,
  products: PropTypes.array,
};

ProductList.defaultProps = {
  setModalState: {},
  setIsOpenModalFirst: false,
  addToFavoritePage: () => {},
  products: [],
};

export default ProductList;
