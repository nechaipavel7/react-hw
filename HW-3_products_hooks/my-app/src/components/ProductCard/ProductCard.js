import React from "react";
import styles from "./ProductCard.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

const ProductCard = (props) => {
  const {
    setModalState,
    setIsOpenModalFirst,
    addToFavoritePage,
    id,
    name,
    price,
    url,
    artcl,
    color,
    isFavorite,
  } = props;
  return (
    <div className={styles.productCard}>
      <button
        type="button"
        className={styles.favouriteBtn}
        onClick={() => {
          addToFavoritePage({ id, name, price, url, artcl, color, isFavorite });
        }}
      >
        <img
          className={styles.favouriteImg}
          src={
            isFavorite ? "./images/favorite-check.png" : "./images/favorite.png"
          }
          alt="star"
        />
      </button>
      <a>
        <img className={styles.productImg} src={url} alt={name} />
      </a>
      <span className={styles.productName}>{name}</span>
      <div className={styles.productBlock}>
        <div className={styles.productColor}>Колір: {color}</div>
        <div className={styles.productArtcl}>Артикул: {artcl}</div>
      </div>
      <div className={styles.productPrise}>{price}</div>
      <div className={styles.productBtn}>
        <Button
          onClick={() => {
            setModalState({ id, name, price, url });
            setIsOpenModalFirst(true);
          }}
          text="КУПИТИ"
          backgroundColor="#001542"
        />
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  setModalState: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  addToFavoritePage: PropTypes.func,
  id: PropTypes.number,
  name: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  url: PropTypes.string,
  artcl: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  addToFavorites: PropTypes.func,
  statusModal: PropTypes.func,
};

ProductCard.defaultProps = {
  setModalState: {},
  setIsOpenModalFirst: false,
  addToFavoritePage: () => {},
  id: 0,
  name: "Назва товару",
  price: "0",
  url: "фото товару",
  artcl: 0,
  color: "Колір товару",
  isFavorite: false,
  addToFavorites: () => {},
  statusModal: () => {},
};

export default ProductCard;
