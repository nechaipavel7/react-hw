import { useState, useEffect } from "react";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import AppRoutes from "./AppRoutes";

const App = () => {
  const [products, setProducts] = useState([]);
  const [isOpenModalFirst, setIsOpenModalFirst] = useState(false);
  const [cartCounter, setCartCounter] = useState(0);
  const [favoriteCounter, setFavoriteCounter] = useState(0);
  const [carts, setCarts] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [modalState, setModalState] = useState({});

  const addToFavoritePage = ({
    id,
    name,
    price,
    url,
    artcl,
    color,
    isFavorite,
  }) => {
    setProducts((prev) => {
      const products = [...prev];
      const index = products.findIndex((el) => el.id === id);

      if (!products[index].isFavorite) {
        products[index].isFavorite = true;
        localStorage.setItem("products", JSON.stringify(products));

        setFavoriteCounter((prev) => {
          prev += 1;
          localStorage.setItem("favoriteCounter", prev);
          return prev;
        });

        setFavorites((prev) => {
          const products = [...prev];
          products.push({
            id,
            name,
            price,
            url,
            artcl,
            color,
            isFavorite: true,
          });

          localStorage.setItem("favorites", JSON.stringify(products));
          return products;
        });
      } else {
        products[index].isFavorite = false;
        localStorage.setItem("products", JSON.stringify(products));

        setFavoriteCounter((prev) => {
          prev -= 1;
          localStorage.setItem("favoriteCounter", prev);
          return prev;
        });

        setFavorites((prev) => {
          const products = [...prev];
          const index = favorites.findIndex((el) => el.id === id);
          products.splice(index, 1);

          localStorage.setItem("favorites", JSON.stringify(products));
          return products;
        });
      }
      return products;
    });
  };

  const addToCartsPage = (card) => {
    const index = carts.findIndex((el) => el.id === card.id);
    const currentCart = carts;

    if (index === -1) {
      card.count = 1;
      currentCart.push(card);
      setCarts(currentCart);
    } else {
      currentCart[index].count += 1;
    }

    const cartCounter = currentCart.reduce(
      (sum, current) => sum + current.count,
      0
    );

    setCartCounter(cartCounter);
    localStorage.setItem("cartCounter", JSON.stringify(cartCounter));
    localStorage.setItem("carts", JSON.stringify(currentCart));
  };

  const incrementCartItem = (id) => {
    setCarts((current) => {
      const carts = [...current];

      const index = carts.findIndex((el) => el.id === id);

      if (index !== -1) {
        carts[index].count += 1;
      }

      const cartCounter = carts.reduce(
        (sum, current) => sum + current.count,
        0
      );
      setCartCounter(cartCounter);
      localStorage.setItem("cartCounter", JSON.stringify(cartCounter));

      localStorage.setItem("carts", JSON.stringify(carts));
      return carts;
    });
  };

  const dicrementCartItem = (id) => {
    setCarts((current) => {
      const carts = [...current];

      const index = carts.findIndex((el) => el.id === id);

      if (index !== -1 && carts[index].count > 1) {
        carts[index].count -= 1;
      }

      const cartCounter = carts.reduce(
        (sum, current) => sum + current.count,
        0
      );
      setCartCounter(cartCounter);
      localStorage.setItem("cartCounter", JSON.stringify(cartCounter));

      localStorage.setItem("carts", JSON.stringify(carts));
      return carts;
    });
  };

  const deleteCartItem = (id) => {
    setCarts((current) => {
      const carts = [...current];

      const index = carts.findIndex((el) => el.id === id);

      if (index !== -1) {
        carts.splice(index, 1);
      }

      const cartCounter = carts.reduce(
        (sum, current) => sum + current.count,
        0
      );
      setCartCounter(cartCounter);
      localStorage.setItem("cartCounter", JSON.stringify(cartCounter));

      localStorage.setItem("carts", JSON.stringify(carts));
      return carts;
    });
  };

  useEffect(() => {
    const getData = async () => {
      const products = localStorage.getItem("products");
      if (products) {
        setProducts(JSON.parse(products));
      } else {
        const products = await fetch("./products.json").then((res) =>
          res.json()
        );
        localStorage.setItem("products", JSON.stringify(products));
        setProducts(products);
      }
    };
    getData();

    const cartCounter = localStorage.getItem("cartCounter");
    if (cartCounter) {
      setCartCounter(JSON.parse(cartCounter));
    }

    const carts = localStorage.getItem("carts");
    if (carts) {
      setCarts(JSON.parse(carts));
    }

    const favoriteCounter = localStorage.getItem("favoriteCounter");
    if (favoriteCounter) {
      setFavoriteCounter(JSON.parse(favoriteCounter));
    }

    const favorites = localStorage.getItem("favorites");
    if (favorites) {
      setFavorites(JSON.parse(favorites));
    }
  }, []);

  return (
    <div className={styles.App}>
      <Header cartCounter={cartCounter} favoriteCounter={favoriteCounter} />
      <main>
        <section>
          <AppRoutes
            modalState={modalState}
            setModalState={setModalState}
            isOpenModalFirst={isOpenModalFirst}
            setIsOpenModalFirst={setIsOpenModalFirst}
            addToFavoritePage={addToFavoritePage}
            deleteCartItem={deleteCartItem}
            dicrementCartItem={dicrementCartItem}
            incrementCartItem={incrementCartItem}
            products={products}
            addToCartsPage={addToCartsPage}
            carts={carts}
            favorites={favorites}
          />
        </section>
      </main>
    </div>
  );
};

export default App;
