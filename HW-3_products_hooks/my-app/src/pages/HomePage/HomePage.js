import React from "react";
import ProductList from "../../components/ProductList/ProductList";
import PropTypes from "prop-types";
import Modal from "../../components/Modal/Modal";

const HomePage = (props) => {
  const {
    modalState,
    setModalState,
    setIsOpenModalFirst,
    isOpenModalFirst,
    addToFavoritePage,
    addToCartsPage,
    products,
  } = props;

  return (
    <>
      <ProductList
        setModalState={setModalState}
        setIsOpenModalFirst={setIsOpenModalFirst}
        isOpenModalFirst={isOpenModalFirst}
        addToFavoritePage={addToFavoritePage}
        products={products}
      />

      <Modal
        text="Додати товар до 'КОШИКА'?"
        confirmBtnAction={addToCartsPage}
        setIsOpenModalFirst={setIsOpenModalFirst}
        modalState={modalState}
        isOpenModalFirst={isOpenModalFirst}
      />
    </>
  );
};

HomePage.propTypes = {
  modalState: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  setModalState: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  isOpenModalFirst: PropTypes.bool,
  addToFavoritePage: PropTypes.func,
  addToCartsPage: PropTypes.func,
  products: PropTypes.array,
};

HomePage.defaultProps = {
  modalState: {},
  setModalState: {},
  setIsOpenModalFirst: false,
  isOpenModalFirst: false,
  addToFavoritePage: () => {},
  addToCartsPage: () => {},
  products: [],
};

export default HomePage;
