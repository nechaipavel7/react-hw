import React from "react";
import FavoriteList from "../../components/FavoriteList/FavoriteList";
import PropTypes from "prop-types";
import Modal from "../../components/Modal/Modal";

const FavoritePage = (props) => {
  const {
    isOpenModalFirst,
    modalState,
    addToCartsPage,
    setIsOpenModalFirst,
    setModalState,
    favorites,
    addToFavoritePage,
  } = props;

  return (
    <>
      <h1>ОБРАНЕ</h1>
      <FavoriteList
        setIsOpenModalFirst={setIsOpenModalFirst}
        setModalState={setModalState}
        favorites={favorites}
        addToFavoritePage={addToFavoritePage}
      />

      <Modal
        text="Додати товар до 'КОШИКА'?"
        confirmBtnAction={addToCartsPage}
        setIsOpenModalFirst={setIsOpenModalFirst}
        modalState={modalState}
        isOpenModalFirst={isOpenModalFirst}
      />
    </>
  );
};

FavoritePage.propTypes = {
  isOpenModalFirst: PropTypes.bool,
  modalState: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  addToCartsPage: PropTypes.func,
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  setModalState: PropTypes.oneOfType([PropTypes.array, PropTypes.func]),
  favorites: PropTypes.array,
  addToFavoritePage: PropTypes.func,
};
FavoritePage.defaultProps = {
  isOpenModalFirst: false,
  modalState: {},
  addToCartsPage: () => {},
  setIsOpenModalFirst: false,
  setModalState: {},
  favorites: [],
  addToFavoritePage: () => {},
};

export default FavoritePage;
