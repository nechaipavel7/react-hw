import React from "react";
import CartList from "../../components/CartList/CartList";
import Modal from "../../components/Modal/Modal";
import PropTypes from "prop-types";

const CartPage = (props) => {
  const {
    modalState,
    setModalState,
    setIsOpenModalFirst,
    isOpenModalFirst,
    deleteCartItem,
    dicrementCartItem,
    incrementCartItem,
    carts,
  } = props;

  return (
    <>
      <h1>КОШИК</h1>
      <CartList
        setModalState={setModalState}
        setIsOpenModalFirst={setIsOpenModalFirst}
        deleteCartItem={deleteCartItem}
        dicrementCartItem={dicrementCartItem}
        incrementCartItem={incrementCartItem}
        carts={carts}
      />
      <Modal
        text="ВИДАЛИТИ цей товар?"
        confirmBtnAction={deleteCartItem}
        setIsOpenModalFirst={setIsOpenModalFirst}
        modalState={modalState}
        isOpenModalFirst={isOpenModalFirst}
      />
    </>
  );
};

CartPage.propTypes = {
  modalState: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  setModalState: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  setIsOpenModalFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  isOpenModalFirst: PropTypes.bool,
  deleteCartItem: PropTypes.func,
  dicrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  carts: PropTypes.array,
};

CartPage.defaultProps = {
  modalState: {},
  setModalState: {},
  setIsOpenModalFirst: false,
  isOpenModalFirst: false,
  deleteCartItem: () => {},
  dicrementCartItem: () => {},
  incrementCartItem: () => {},
  carts: [],
};

export default CartPage;
