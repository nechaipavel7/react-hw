import React from "react";
import CartList from "../../components/CartList/CartList";
import Modal from "../../components/Modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import { deleteCartItemAC } from "../../store/cart/actionCreators";

const CartPage = () => {
  const dispatch = useDispatch();
  const modalData = useSelector((store) => store.modal.modalData);

  return (
    <>
      <h1>КОШИК</h1>
      <CartList />
      <Modal
        text="ВИДАЛИТИ цей товар?"
        confirmBtnAction={() => dispatch(deleteCartItemAC(modalData))}
      />
    </>
  );
};

export default CartPage;
