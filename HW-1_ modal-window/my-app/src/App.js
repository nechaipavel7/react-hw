import { Component } from "react";
import styles from "./App.module.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
  state = {
    isOpenModalFirst: false,
    isOpenModalSecond: false,
  };

  render() {
    const { isOpenModalFirst, isOpenModalSecond } = this.state;
    return (
      <div className={styles.App}>
        <Button
          text="Open first modal"
          onClick={() => {
            this.setState({ isOpenModalFirst: true });
          }}
          backgroundColor="#3A289E"
        />

        <Button
          text="Open second modal"
          onClick={() => {
            this.setState({ isOpenModalSecond: true });
          }}
          backgroundColor="#4FC238"
        />

        {isOpenModalFirst && (
          <Modal
            closeButton="X"
            onClick={() => {
              this.setState({ isOpenModalFirst: false });
            }}
            header="Do you want to delete this file?"
            text="Once you delete this file. Are you sure you want to delete it?"
            action={
              <div>
                <Button text="CANCEL" backgroundColor="#9E4439" />
                <Button text="OK" backgroundColor="#3A289E" />
              </div>
            }
          ></Modal>
        )}

        {isOpenModalSecond && (
          <Modal
            closeButton="X"
            onClick={() => {
              this.setState({ isOpenModalSecond: false });
            }}
            header="Do you want to edit this file?"
            text="Once you edit this file. Are you sure you want to edit it?"
            action={
              <div>
                <Button text="CANCEL" backgroundColor="#9E4439" />
                <Button text="EDIT FILE" backgroundColor="#4FC238" />
                <Button text="CREATE NEW FILE" backgroundColor="#12B891" />
              </div>
            }
          ></Modal>
        )}
      </div>
    );
  }
}

export default App;
