import React from "react";
import styles from "./Button.module.scss";

class Button extends React.Component {
  render() {
    const { text, onClick, backgroundColor } = this.props;

    return (
      <button
        className={styles.btn}
        onClick={onClick}
        type="button"
        style={{ backgroundColor: backgroundColor }}
      >
        {text}
      </button>
    );
  }
}

export default Button;
