import React from "react";
import styles from "./Modal.module.scss";

class Modal extends React.Component {
  render() {
    const { header, text, closeButton, action, onClick } = this.props;

    return (
      <div>
        <div className={styles.backgroundModal} onClick={onClick}></div>
        <div className={styles.modal}>
          <div className={styles.header}>
            <h1 className={styles.headerText}>{header}</h1>
            <button className={styles.closeButton} onClick={onClick}>
              {closeButton}
            </button>
          </div>
          <div>
            <h2 className={styles.textModal}>{text}</h2>
            <div>{action}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
